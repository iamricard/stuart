const redux = require('redux')
const thunk = require('redux-thunk').default
const duck = require('./duck')
const INITIAL_STATE = require('./initial-state')

module.exports = redux.createStore(
  duck.reducer,
  INITIAL_STATE,
  redux.applyMiddleware(thunk)
)

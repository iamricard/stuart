const prompt = require('prompt')
const store = require('./store')
const duck = require('./duck')

const promptSchema = [
  { name: 'XCoord', required: true, type: 'integer' },
  { name: 'YCoord', required: true, type: 'integer' }
]

function loop (winner) {
  if (winner) return console.log(`Player ${store.getState().winner} won!`)

  prompt.start()
  const { targetPlayer } = store.getState()
  const player = targetPlayer === 'blue' ? 'red' : 'blue'
  console.log(`${player} player's turn`)
  prompt.get(promptSchema, (err, result) => {
    store.dispatch(duck.fire(targetPlayer, [+result.XCoord, +result.YCoord]))

    loop(store.getState().winner)
  })
}

loop(false)

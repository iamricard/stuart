const _ = require('lodash')

const CHECK_LIVES = 'CHECK_LIVES'
const FIRE = 'FIRE'
const checkLives = () => ({ type: CHECK_LIVES })
const fired = (player, target) => ({
  type: FIRE,
  payload: { target, player }
})
const fire = (player, target) => {
  return (dispatch) => {
    dispatch(fired(player, target))
    dispatch(checkLives())
  }
}

function player (state, action) {
  if (action.type === FIRE) {
    return Object.assign(state, {
      boats: _.compact(state.boats.filter((boat) => {
        return !_.isEqual(boat, action.payload.target)
      }))
    })
  }

  return state
}

function isDead (player) {
  return !player.boats.length
}

function reducer (state = {}, action) {
  if (action.type === FIRE) {
    return Object.assign(state, {
      targetPlayer: state.targetPlayer === 'blue' ? 'red' : 'blue',
      [action.payload.player]: player(state[action.payload.player], action)
    })
  } else if (action.type === CHECK_LIVES) {
    return Object.assign(state, {
      winner: (isDead(state.blue) && 'red') || (isDead(state.red) && 'blue')
    })
  }

  return state
}

exports.reducer = reducer
exports.fire = fire
